$composer create-project --prefer-dist laravel/laravel API-resources
$chmod -R 777 storage
$php artisan make:migration create_articles_table --create=articles

*databases/seeds/ArticlesTableSeeder
$php artisan make:seeder ArticlesTableSeeder

*databases/seeds/DatabaseSeeder
$php artisan make:factory ArticlesFactory

$php artisan make:model Article 
$php artisan migrate

$php artisan db:seed //if error run "composer dump-autoload"
$php artisan make:controller ArticleController --resource //create file articleController
$php artisan make:resource Article



